"""Basic program flow

    Your game loop code should look a little like this.
    It's not exact code but hopefully enough to get you moving
    without spoiling the fun of creating it. (Not every detail is shown)

    Bonus points if you address the possibility of a tie after three rounds.
"""


def main():
    print_header()

    rolls = build_the_three_rolls()

    name = get_players_name()

    player1 = Player(name)
    player2 = Player("computer")

    game_loop(player1, player2, rolls)


def game_loop(player1, player2, rolls):
    count = 1
    while count < 3:
        p2_roll = None  # TODO: get random roll
        p1_roll = None  # TODO: have player choose a roll

        outcome = p1_roll.can_defeat(p2_roll)

        # display throws
        # display winner for this round

        count += 1

    # Compute who won

"""Rock Paper Scissors Game
    https://www.wikihow.com/Play-Rock,-Paper,-Scissors

    You will model Rock, Paper, Scissors with classes and a game loop.
    Recall the three rolls (Rock, Paper, and Scissors). These can be just hard coded into your game.
"""


class Roll:
    """ You need to add the following:
       * name of roll
       * rolls that can be defeated by self
       * rolls that defeated self
    """
    pass


class Player:
    """ This object would have only:
     * name of player
    """
    pass

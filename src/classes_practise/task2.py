"""
In this task you are presented with another class, User this time.

Like last task you are asked to complete it, see the TODOs in the code below:

* Complete the get_full_name property (more on properties here) that prints first and last name separated by a space.
* Complete the username property following its docstring.
* Complete the special representation dunder methods: __str__ and __repr__. Look at the tests what they should return.

Useful links:
https://dbader.org/blog/python-dunder-methods
https://stackoverflow.com/a/1438297

Good luck and keep calm and code in Python!
"""


class User:
    """A User class
       (Inspired by Django's User model)
    """

    def __init__(self, first_name, last_name):
        """Constructor, base values"""
        self.first_name = first_name
        self.last_name = last_name

    @property
    def get_full_name(self):
        """Return first separated by a whitespace
           and using title case for both.
        """
        pass

    @property
    def username(self):
        """A username consists of the first char of
           the user's first_name and the first 7 chars
           of the user's last_name, both lowercased.
        """
        pass

    # TODO 3: you code
    #
    # add a __str__ and a __repr__
    # see: https://stackoverflow.com/a/1438297
    # "__repr__ is for devs, __str__ is for customers"
    #
    # see also TESTS for required output

    def __str__(self):
        pass

    def __repr__(self):
        """Don't hardcode the class name, hint: use a
           special attribute of self.__class__ ...
        """
        pass
